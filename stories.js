document.addEventListener("DOMContentLoaded", function() {
    stories.reverse();
    stories.forEach(function(story) {
        var content = `<h1>${story.title}</h1>${story.content}`;
        vandy.UI.renderDiv(content, document.querySelector('body'));
    });
});
