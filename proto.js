document.addEventListener("DOMContentLoaded", function() {
    renderContent(stories);
    initButtons();
});


var renderContent = function(stories) {
    if (stories.length === 1) {
        renderStory(stories);
    } else if (stories.length >= 2) {
        renderStories(stories);
    } else {
        noContent();
    }
}

var renderStories = function(stories) {
    stories.reverse();
    stories.forEach(function(story) {
        story.id = getId(story);
        var content = `
        <img src="https://placehold.it/320/240">
        <h1>${story.title}</h1>
        <div class="buttons">
            <a href="#" class="button save" data-id="${story.id}">ICON</a>
            <a href="#" class="button read" data-id="${story.id}">READ</a>
        </div>
        `;
        vandy.UI.renderDiv(content, document.querySelector('main'), 'story', story.id);
    });
}

var renderStory = function(stories) {
    stories.forEach(function(story) {
        var content = `<h1>${story.title}</h1>${story.content}`;
        vandy.UI.renderDiv(content, document.querySelector('main'), ['story', 'single'], getId(story));
    });
}

var noContent = function() {
    var content = `Geen content`;
    vandy.UI.renderDiv(content, document.querySelector('main'));
}

var getId = function(story) {
    var idString = story.guid['#text'];
    var id = idString.split("?p=")[1];
    return id;
}

var getStoryById = function(id) {
    var value;
    stories.forEach(function(story) {
        var storyId = getId(story);
        if (storyId === id) {

            value = story;
        }
    });
    return value;
}

var initButtons = function() {
    var buttons = document.querySelectorAll('a');
    buttons.forEach(function(button) {
        if (button.classList.contains("button")) {
            if (button.classList.contains("save")) {
                setSaveButton(button);
            } else if (button.classList.contains("read")) {
                setReadButton(button);
            }
        }
    });
}

var setSaveButton = function(button) {
    button.addEventListener('click', function(e) {
        e.preventDefault();
        var button = e.target;
        var id = button.dataset.id;
    });
}

var setReadButton = function(button) {
    button.addEventListener('click', function(e) {
        e.preventDefault();
        var button = e.target;
        var id = button.dataset.id;
        clearMain();
        renderStory([getStoryById(id)]);
    });
}

var clearMain = function() {
    document.querySelector("main").innerHTML = null;
}